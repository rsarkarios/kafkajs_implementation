const { Kafka } = require('kafkajs');

const kafka = new Kafka({
    clientId: 'my-app',
    brokers: ['localhost:9093']
})

const consumer = kafka.consumer({ groupId: 'test-group3' ,})


const consumerFun = async () => {
    await consumer.connect()
    await consumer.subscribe({ topic: 'test-topic', fromBeginning: true })
    await consumer.run({
        eachBatchAutoResolve: true,
        eachMessage: async ({ topic, partition, message }) => {
          console.log({
            partition,
            offset: message.offset,
            value: message.value.toString(),
          })
        },
      })
    // await consumer.run({
    //     eachBatchAutoResolve: true,
    //     eachBatch: async ({
    //         batch,
    //         resolveOffset,
    //         heartbeat,
    //         commitOffsetsIfNecessary,
    //         uncommittedOffsets,
    //         isRunning,
    //         isStale,
    //     }) => {
    //         for (let message of batch.messages) {
    //             console.log({
    //                 topic: batch.topic,
    //                 partition: batch.partition,
    //                 highWatermark: batch.highWatermark,
    //                 message: {
    //                     offset: message.offset,
    //                     value: message.value.toString(),
    //                 }
    //             })
    
    //             resolveOffset(message.offset)
    //             await heartbeat()
    //         }
    //     },
    // })
}


consumerFun()
.then(s => console.log("received3"))
.catch(err=> console.log(err))