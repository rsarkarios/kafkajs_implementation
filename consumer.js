const { Kafka } = require('kafkajs');

const kafka = new Kafka({
    clientId: 'my-app',
    brokers: ['localhost:9092', 'localhost:9093'],
})

const consumer = kafka.consumer({ groupId: 'test-group1'})


const consumerFun = async () => {
    await consumer.connect()
  await consumer.subscribe({ topic: 'topic-c', fromBeginning: true })
  await consumer.subscribe({ topic: 'topic-a', fromBeginning: true });
    // await consumer.run({
    //     eachBatchAutoResolve: true,
    //     partitionsConsumedConcurrently: 3, 
    //     eachMessage: async ({ topic, partition, message }) => {
    //       console.log({
    //         partition,
    //         offset: message.offset,
    //         value: message.value.toString(),
    //       })
    //     },
    //   })
    await consumer.run({
      eachBatchAutoResolve: true,
      partitionsConsumedConcurrently: 3,
        eachBatch: async ({
            batch,
            resolveOffset,
            heartbeat,
            commitOffsetsIfNecessary,
            uncommittedOffsets,
            isRunning,
            isStale,
        }) => {
          for (let message of batch.messages) {
              
                console.log({
                    topic: batch.topic,
                    partition: batch.partition,
                    highWatermark: batch.highWatermark,
                    message: {
                        offset: message.offset,
                        value: message.value.toString(),
                    }
                })
    
                resolveOffset(message.offset)
                await heartbeat()
            }
        },
    })
}


consumerFun()
.then(s => console.log("received"))
.catch(err=> console.log(err))