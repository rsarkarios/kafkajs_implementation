const { Kafka } = require('kafkajs');
const fs = require('fs');
const cron = require('node-cron')

const kafka = new Kafka({
    clientId: 'my-app',
    brokers: ['localhost:9092', 'localhost:9093']
})
const producer = kafka.producer()

// const producerFunc = async (msg) => {
//     console.log(msg)
//     // Producing
//     await producer.connect()
//     await producer.send({
//         topic: 'test-topic',
//         messages: msg.parametes
//     })
// }

// cron.schedule('0 */1 * * * *', () => {
//     console.log("Hits in every 1 miniute .....")
//     fs.readFile('content.json', (err, data) => {
//       if (err) console.log(err);
//       let parseddata = JSON.parse(data);
//       producerFunc(parseddata)    
//     }) 
//   });

const producerFunc = async () => {
    // Producing
    await producer.connect()
    await producer.send({
        topic: 'test-topic',
        messages: [{ key: 'key1', value: 'hello world2' },
        { key: 'key2', value: 'hey hey2!' }]
    })
        .then(re => {
        console.log(re)
    })
    
}
 

producerFunc()
    .then(s => console.log("sent >>>"))
.catch(err=> console.log(err))