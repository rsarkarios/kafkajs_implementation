const { Kafka } = require('kafkajs');

const kafka = new Kafka({
    clientId: 'my-app',
    brokers: ['localhost:9093']
})

const consumer = kafka.consumer({ groupId: 'test-group2' ,})


const consumerFun = async () => {
    await consumer.connect()
    await consumer.subscribe({ topic: 'test-topic', fromBeginning: true })
    await consumer.run({
        eachBatchAutoResolve: true,
        eachMessage: async ({ topic, partition, message }) => {
          console.log({
            partition,
            offset: message.offset,
            value: message.value.toString(),
          })
        },
      })
}


consumerFun()
.then(s => console.log("received2"))
.catch(err=> console.log(err))